#ifndef __orov__msg__control__TridentControl__idl__
#define __orov__msg__control__TridentControl__idl__

module orov {
module msg {
module control {

    // Control Types
    enum EControlType
    {
        PERCENT_OF_MAX = 0,
        RADIANS_PER_SEC = 1
    };

    // Low-level motor commands. These get directly mapped to an equivalent Mavlink message and forwarded to each ESC 
    struct TridentMotorCommand
    {
        // Vehicle ID
        string id; //@key

        // The type of control we are using
        EControlType controlType;

        // Motor values
        float portCmd; // The value to send to the port motor
        float verticalCmd; // The value to send to the vertical motor
        float starboardCmd; // The value to send to the starboard motor
    };
    
    // Request motor configuration 
    struct TridentMotorConfigRequest
    {
        string id; // Not used at present
    };
    
    struct TridentMotorConfig
    {
        // MCU ID
        string id; //@key

        // Motor values
        float minSpeed; // Minimum speed error
        float maxSpeed; // Maximum speed error
        long timeout;   // Timeout in milliseconds
    };
    

    // High-level control commands issued by a user or other high-level control system. Units vary depending on current control mode.
    // E.g., pitch could be a target position in rads, while yaw is a target yaw rate in rads/sec
    struct TridentControlTarget
    {
        string id;      //@key
        
        float pitch;
        float yaw;
        float thrust;
        float lift;
    };
    
    const long MAX_TARGET_LEN = 100;
    
    // List of Control targets and lengths of time    
    struct TridentCruiseControlTarget
    {
        sequence<unsigned long, MAX_TARGET_LEN> times;                     // Times in milliseconds
        sequence<TridentControlTarget, MAX_TARGET_LEN> targets;  // Target above
    };

    // NOTE: Not sure if these are actually used anywhere
    struct RateControlParameters
    {
        // Vehicle ID
        string vehicleID; //@key 

        // The controller we are sending these parameters to
        // TODO: Should these be enumed out? String for now as we prototype
        string controllerID; //@key

        // The rate controller is described as
        // y_dot = k*x^n
        // where x is the control input from the client device
        float k;
        float n;
    };

};
};
};

#endif
