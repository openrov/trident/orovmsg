import os
import sys
import glob
import errno
import subprocess
import time
import argparse
import shutil

# Utility method that enables using include patterns with shutil.copytree
# From: https://stackoverflow.com/a/35161407
import fnmatch
import os

def include_patterns(*patterns):
    """Factory function that can be used with copytree() ignore parameter.

    Arguments define a sequence of glob-style patterns
    that are used to specify what files to NOT ignore.
    Creates and returns a function that determines this for each directory
    in the file hierarchy rooted at the source directory when used with
    shutil.copytree().
    """
    def _ignore_patterns(path, names):
        keep = set(name for pattern in patterns
                            for name in fnmatch.filter(names, pattern))
        # Determine file names which DIDN'T match any of the patterns given.
        ignore = set(name for name in names
                        if name not in keep and not os.path.isdir(os.path.join(path, name)))
        return ignore
    return _ignore_patterns

def generate_java( gen_command, src_dir, dst_dir, java_pkg ):
    # Set options
    gen_path = os.path.join( dst_dir )
    gen_args = [ "-language", "Java" ]

    if( java_pkg != "" ):
        gen_args += [ "-package", java_pkg ]
    
    # Cleanup
    if( os.path.exists( gen_path ) and os.path.isdir( gen_path ) ):
        print( "Deleting previously generated files before generating new files" )
        shutil.rmtree( gen_path )

    # Create gen_path dir
    os.makedirs( gen_path )

    # Generate
    for idl in idl_files:
        # Call the generation tool
        subprocess.check_call( 
            [ gen_command ] + 
            gen_args +
            [ 
                "-I", src_dir,
                "-d", gen_path,
                idl
            ]
        )

def generate_xml( gen_command, src_dir, dst_dir ):
    # Set options
    gen_path = os.path.join( dst_dir )
    gen_args = [ "-convertToXml" ]

    # Cleanup
    if( os.path.exists( gen_path ) and os.path.isdir( gen_path ) ):
        print( "Deleting previously generated files before generating new files" )
        shutil.rmtree( gen_path )

    # Create gen_path dir
    os.makedirs( gen_path )

    # Generate
    for idl in idl_files:
        # Call the generation tool
        subprocess.check_call( 
            [ gen_command ] + 
            gen_args +
            [ 
                "-I", src_dir,
                "-d", gen_path,
                idl
            ]
        )

def generate_cpp( gen_command, src_dir, dst_dir ):
    # Set options
    gen_path = os.path.join( dst_dir )
    gen_path_src = os.path.join( gen_path, "src" )
    gen_path_inc = os.path.join( gen_path, "include" )
    gen_args = [ "-language", "C++11", "-stl" ]

    # Cleanup
    print( "Deleting previously generated files before generating new files" )
    if( os.path.exists( gen_path_src ) ):
        shutil.rmtree( gen_path_src )
    if( os.path.exists( gen_path_inc ) ):
        shutil.rmtree( gen_path_inc )

    for idl in idl_files:
        # Get path to file relative to src_dir
        rel_path    = os.path.relpath( idl, src_dir )
        rel_dir     = os.path.dirname( rel_path )
        out_dir     = os.path.join( gen_path_src, rel_dir )

        # Create the relative dir structure in the dst_dir
        os.makedirs( out_dir, exist_ok=True )

        # Call the generation tool
        subprocess.check_call( 
            [ gen_command ] + 
            gen_args +
            [ 
                "-I", src_dir,
                "-d", out_dir,
                idl
            ]
        )

    # Split out a copy of the headers for consumers of the library
    shutil.copytree( gen_path_src, gen_path_inc, ignore=include_patterns( "*.h", "*.hpp", "*.hxx" ) )

if __name__ == "__main__":
    # Set up argument parsing
    arg_parser = argparse.ArgumentParser( description="Script for generating DDS types using a specified gentool (supports only rtiddsgen for now)." )
    arg_parser.add_argument( "--gentooldir", type=str, required=True, help="Path containing code generation binary (e.g. /usr/local/bin)" )
    arg_parser.add_argument( "--gentool", type=str, required=True, help="Name of code generation binary (e.g. rtiddsgen)" )
    arg_parser.add_argument( "--src", type=str, required=True, help="Top level directory containing IDL files" )
    arg_parser.add_argument( "--dst", type=str, required=True, help="Directory to place generated sources in" )
    arg_parser.add_argument( "--lang", type=str, required=True, help="Which language to generate code for [cpp,java,xml]" )
    arg_parser.add_argument( "--java-package", type=str, required=False, help="Package prefix for java applications, e.g. com.openrov.cockpit" )

    # Parse command line options 
    args = arg_parser.parse_args()

    # Get option values
    gendir      = os.path.abspath( args.gentooldir )
    gentool     = args.gentool
    gen_command = os.path.join( gendir, gentool )
    src_dir     = os.path.abspath( args.src )
    dst_dir     = os.path.abspath( args.dst )
    language    = args.lang
    java_pkg    = args.java_package

    # ===============================
    # Validate option inputs

    # Gentool
    if( gentool not in [ "rtiddsgen_server" ] ):
        raise Exception( "Specified gentool is not supported: {}".format( gentool ) )
    elif( not os.path.exists( gen_command ) ):
        raise Exception( "Specified gentool not found: {}".format( gen_command ) )

    # Source path
    if( ( not os.path.exists( src_dir ) ) and ( os.path.isdir( src_dir ) ) ):
        raise Exception( "Invalid source path: {}".format( src_dir ) )

    # Dest path (if it exists already, make sure it is a dir)
    if( os.path.exists( dst_dir ) and ( not os.path.isdir( dst_dir ) ) ):
        raise Exception( "Invalid dest path: {}".format( dst_dir ) )

    # Get list of IDL files and check if empty
    idl_files = glob.glob( src_dir + "/**/*.idl", recursive=True )
    if( not idl_files ):
        raise Exception( "No IDL files found in: {}".format( src_dir ) )

    # Check language
    if( language not in [ "cpp", "java", "xml" ] ):
        raise Exception( "Invalid language" )

    # ===============================

    print( "Beginning generation of code for IDL in: {}".format( src_dir ) )

    # Generate code based on language
    if( language == "cpp" ):
        generate_cpp( gen_command, src_dir, dst_dir )
    elif( language == "java" ):
        generate_java( gen_command, src_dir, dst_dir, java_pkg )
    elif( language == "xml" ):
        generate_xml( gen_command, src_dir, dst_dir )

    # Finished!
    print( "Finished generating types!" )